<?php
    $http_request = json_decode(file_get_contents("php://input"),true);
    

    if (isset($http_request['N']) )
    {
        $response_len = $http_request['N'];
    }
    else if(isset($http_request['n']))
    {
        $response_len = $http_request['n'];
    } else {
    echo "No connection.";
    exit(1);
}

    //check if input is a number, if not then exit program
    if(!is_numeric($response_len)) {
    echo "Post value not numeric.";
    exit(1);
    }

    // set variable to a string of characters, numbers, and symbols
    $charset = "0123456789!@#&*ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    $set_len = strlen($charset);
    
    //set response to an empty string
    $response = "";

    for($i = 0; $i < $response_len; $i++) {
		$curr_rand = mt_rand(0, $set_len - 1);

		//get random character from string
		$rand_char = substr($charset, $curr_rand , 1);
		$response .= $rand_char;
    }

    $responseArr = array(data => $response);
    $resultJson = json_encode($responseArr);
 
 
 //echo result
echo $resultJson;
?>


