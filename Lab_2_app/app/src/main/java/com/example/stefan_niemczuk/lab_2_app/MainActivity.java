// ID: 005127664
package com.example.stefan_niemczuk.lab_2_app;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.view.View;

public class MainActivity extends AppCompatActivity {


private EditText copyText, pasteTxt;
    private ClipboardManager cbm;
    private ClipData cd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        copyText = (EditText) findViewById(R.id.copyText);
        pasteTxt = (EditText) findViewById(R.id.pasteText2);

        cbm = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
    }
    public void copy (View view) {
        String text = copyText.getText().toString();
        cd = ClipData.newPlainText("text", text);
        cbm.setPrimaryClip(cd);
    }

    public void paste (View view) {
        ClipData cd2 = cbm.getPrimaryClip();
        ClipData.Item item = cd2.getItemAt(0);
        String copied = item.getText().toString();
        pasteTxt.setText(copied);
    }
}
