package com.example.stefan_niemczuk.lab_3;

        import android.support.v7.app.AppCompatActivity;
        import android.os.Bundle;



        import android.os.AsyncTask;
        import android.os.Bundle;
        import android.support.v7.app.AppCompatActivity;
        import android.util.Log;
        import android.view.View;
        import android.widget.EditText;
        import android.widget.TextView;
        import android.widget.Button;

        import org.json.JSONException;
        import org.json.JSONObject;

        import java.io.BufferedInputStream;
        import java.io.BufferedReader;
        import java.io.IOException;
        import java.io.InputStream;
        import java.io.InputStreamReader;
        import java.net.HttpURLConnection;
        import java.net.MalformedURLException;
        import java.net.URL;

public class MainActivity extends AppCompatActivity {

    EditText editText;
    TextView textView;
    String  jsonString;
    String url = "http://date.jsontest.com";
    String result = "";
    String data = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void runTask (View v){
        editText = (EditText) findViewById(R.id.editText);
        jsonString = editText.getText().toString();

        //This is how you execute an asynctask. Create a new
        //instance of the async class and call execute().
        new sendPostRequest().execute(url);
    }


    public class sendPostRequest extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            //Now in a background thread. Do all work with the URL
            //here.

            HttpURLConnection connection = null;


            try {
                //grab url from the first param and instantiate a new URL object
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                int code = connection.getResponseCode();

                if (code == 200) {

                    //get input stream handle
                    InputStream input = new BufferedInputStream(connection.getInputStream());

                    //if input contains something
                    if (input != null) {
                        BufferedReader bReader = new BufferedReader(new InputStreamReader(input));
                        StringBuilder sBuilder = new StringBuilder();

                        //Append everything from the
                        //BufferedReader into a String variable
                        //to use later. Do this in the while loop

                        while (bReader.ready()) {
                            //do something
                            sBuilder.append(bReader.readLine());
                        }
                        input.close();
                        data = sBuilder.toString();
                    }
                }
                //error catchers
            }catch(MalformedURLException e){
                e.printStackTrace();
            }catch(IOException e){
                e.printStackTrace();

            }finally{
                connection.disconnect();
            }

            //Generate a new JSONObject with the data read in
            //from the BufferedReader.
            try {
                Log.d("DATA CONTENTS: ", data );
                JSONObject jsonObject = new JSONObject(data);
                result = jsonObject.getString(jsonString);
            } catch(JSONException e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result) {

            //This is where you will display the String you got
            //from the JSONObject in the text field on the app.
            TextView resultText = (TextView) findViewById(R.id.resultView);
            resultText.setText(result);

        }
    }
}




